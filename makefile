
build-contracts:
	@mkdir -p build
	@rm -rf build/*
	@cd build && cmake .. && make

deploy-contracts:
	$(eval -include .env)
	@cleos wallet unlock --name $(CONTRACTS_STAKINGTOKEN_ACCOUNT) --password $(CONTRACTS_STAKINGTOKEN_PASSWORD) || echo ""
	@cleos -u $(CONTRACTS_CHAIN_ENDPOINT) set contract $(CONTRACTS_STAKINGTOKEN_ACCOUNT) ./build/stakingtoken || echo ""
	@cleos wallet lock --name $(CONTRACTS_STAKINGTOKEN_ACCOUNT)
