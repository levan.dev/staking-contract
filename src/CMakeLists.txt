cmake_minimum_required(VERSION 3.5)
project(stakingtoken)

set(EOSIO_WASM_OLD_BEHAVIOR "Off")
find_package(eosio.cdt)

add_contract( stakingtoken stakingtoken stakingtoken.cpp )
target_include_directories( stakingtoken PUBLIC ${CMAKE_SOURCE_DIR}/../include )
target_ricardian_directory( stakingtoken '${CMAKE_SOURCE_DIR}/../ricardian/' )
