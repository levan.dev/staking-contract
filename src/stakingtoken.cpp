#include <eosio/system.hpp>
#include <math.h>
#include <stakingtoken.hpp>

ACTION stakingtoken::setinflation( uint8_t pct )
{
    require_auth( get_self() );

    check( pct > 0 && pct < 100, "The pct should be less than 100 and greater than 0" );

    global_table glo_sing( get_self(), get_self().value );

    auto g = glo_sing.get_or_create( get_self(), global{ .inflation_pct = pct } );
    g.inflation_pct = pct;
    glo_sing.set( g, get_self() );
}

ACTION stakingtoken::claim( name account, uint64_t index )
{
    // ask permission of user account
    require_auth( account );

    stake_table table( get_self(), get_self().value );
    auto        row = table.find( index );

    // verify that the record exists for the provided index
    check( row != table.end(), "no records found" );

    // the record must belong to the provided account
    check( row->account == account, "stake not belong to the provided account" );

    // status must be STAKE_IN_PROGRESS
    check( row->status == stake_status::STAKE_IN_PROGRESS, "invalid status, must be STAKE_IN_PROGRESS" );

    // verify the payment date has been passed
    check( row->payout_date < current_time_point(), "invalid date, not yet ready to claim" );

    global_table glo_sing( get_self(), get_self().value );

    // calculate the new and additional tokens to issue
    auto new_tokens = row->payout - row->libre_staked;
    auto additional_tokens = glo_sing.get().inflation_pct * new_tokens / 100;

    eosio::name temp_referrer = get_referrer( row->account );
    eosio::name referrer = temp_referrer != eosio::name( -1 ) ? temp_referrer : DEFAULT_REFERRER_ACCOUNT;

    // issue new_tokens
    action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "issue" ),
            std::make_tuple( TOKEN_ISSUER,
                             new_tokens + ( additional_tokens * 2 ),
                             "claim for stake #" + to_string( index ) ) )
        .send();

    // send new_tokens
    action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( TOKEN_ISSUER, row->account, new_tokens, "claim for stake #" + to_string( index ) ) )
        .send();

    // send libre_staked
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( get_self(), row->account, row->libre_staked, "claim for stake #" + to_string( index ) ) )
        .send();

    // send tokens to dao
    action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( TOKEN_ISSUER,
                             DAO_ACCOUNT,
                             additional_tokens,
                             "DAO contribution from: " + account.to_string() ) )
        .send();

    // send tokens to referrer
    action( permission_level{ TOKEN_ISSUER, eosio::name( "stake" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( TOKEN_ISSUER,
                             referrer,
                             additional_tokens,
                             "Bonus from referral's claim: " + account.to_string() ) )
        .send();

    // change the status to STAKE_COMPLETED
    table.modify( row, get_self(), [&]( auto &ref ) { ref.status = stake_status::STAKE_COMPLETED; } );

    action( permission_level{ get_self(), eosio::name( "active" ) },
            EOSIO_CONTRACT,
            EOSIO_CONTRACT_ACTION,
            std::make_tuple( account ) )
        .send();
}

ACTION stakingtoken::unstake( name account, uint64_t index )
{
    // ask permission of user account
    require_auth( account );

    stake_table table( get_self(), get_self().value );
    auto        row = table.find( index );

    // validate records for the index provided
    check( row != table.end(), "no records found" );

    // account must be equal
    check( row->account == account, "account not match" );

    // status must be STAKE_IN_PROGRESS
    check( row->status == stake_status::STAKE_IN_PROGRESS, "status must be STAKE_IN_PROGRESS" );

    // send libre_staked less penalty
    asset penalty = asset( row->libre_staked.amount * UNSTAKE_PENALTY, SUPPORTED_TOKEN_SYMBOL );
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( get_self(),
                             row->account,
                             row->libre_staked - penalty,
                             "unstake penalty " + penalty.to_string() ) )
        .send();

    // send penalty to DAO account
    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( SUPPORTED_TOKEN_CONTRACT ),
            eosio::name( "transfer" ),
            std::make_tuple( get_self(), DAO_ACCOUNT, penalty, "unstake penalty from " + account.to_string() ) )
        .send();

    // change the status to STAKE_CANCELED
    table.modify( row, get_self(), [&]( auto &ref ) { ref.status = stake_status::STAKE_CANCELED; } );

    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( "eosio" ),
            eosio::name( "vonstake" ),
            std::make_tuple( account ) )
        .send();
}

ACTION stakingtoken::stakepreview( name           account,
                                   time_point_sec stake_date,
                                   uint64_t       stake_length,
                                   float          mint_bonus,
                                   asset          libre_staked )
{
    // ask permission of user account
    require_auth( account );

    // libre_staked symbol must match the supported token symbol
    check( libre_staked.symbol == SUPPORTED_TOKEN_SYMBOL, "Invalid token symbol" );

    // stake_length must be at least 1
    check( stake_length >= 1, "You must stake for at least 1 day" );

    // stake_length must be at least 1
    check( stake_length <= 1460, "You can stake up to 4 years" );

    const float          apy = calculate_apy( stake_date, stake_length, mint_bonus / 100 );
    const asset          payout = calculate_payout( apy, stake_length, libre_staked );
    const time_point_sec payout_date = stake_date + days( stake_length );

    SEND_INLINE_ACTION( *this,
                        stakelog,
                        { { get_self(), name( "active" ) } },
                        { account,
                          stake_date,
                          stake_length,
                          mint_bonus,
                          libre_staked,
                          apy * 100,
                          payout,
                          payout_date,
                          stake_status::STAKE_IN_PROGRESS } );
}

ACTION stakingtoken::stakelog( name           account,
                               time_point_sec stake_date,
                               uint64_t       stake_length,
                               float          mint_bonus,
                               asset          libre_staked,
                               float          apy,
                               asset          payout,
                               time_point_sec payout_date,
                               uint64_t       status )
{
    // ask permission of self account
    require_auth( get_self() );
}

ACTION stakingtoken::mintpreview( name account, time_point_sec mint_date, uint64_t mint_length, asset btc_contributed )
{
    // ask permission of user account
    require_auth( account );

    // btc_contributed symbol must match the supported token symbol
    check( btc_contributed.symbol == SUPPORTED_MINT_TOKEN_SYMBOL, "Invalid token symbol" );

    // mint_length must be at least 1
    check( mint_length >= 1, "You must stake for at least 1 day" );

    // mint_length must be at least 1
    check( mint_length <= 1460, "You can stake up to 4 years" );

    float day_of_mint_rush = get_days_diff( mint_date, M_L );
    float mint_bonus = calculate_mint_bonus( day_of_mint_rush );
    float mint_apy = calculate_mint_apy( mint_length, mint_bonus );

    SEND_INLINE_ACTION(
        *this,
        mintlog,
        { { get_self(), name( "active" ) } },
        { account, day_of_mint_rush, mint_length, btc_contributed, mint_bonus * 100, asset(), mint_apy * 100 } );
}

ACTION stakingtoken::mintlog( name     account,
                              uint64_t day_of_mint_rush,
                              uint64_t staked_days,
                              asset    btc_contributed,
                              float    mint_bonus,
                              asset    libre_minted,
                              float    mint_apy )
{
    // ask permission of self account
    require_auth( get_self() );
}

ACTION stakingtoken::mintprocess()
{
    // ask permission of self account
    require_auth( get_self() );

    // @todo: validate this action not be executed before the end of the mint rush

    mint_bonus_t mint_table( get_self(), get_self().value );
    auto         status_index = mint_table.get_index< name( "status" ) >();
    int          index = 0;

    for ( auto itr = status_index.lower_bound( mint_status::MINT_IN_PROGRESS );
          itr != status_index.upper_bound( mint_status::MINT_IN_PROGRESS );
          itr++ )
    {
        index++;

        mintstats_t stats_table( get_self(), get_self().value );
        auto        stats_info = stats_table.get_or_create( get_self() );

        const float libre =
            ( (float)itr->btc_contributed.amount ) / ( (float)stats_info.btc_contributed.amount ) * TOTAL_LIBRE_TOKEN;
        const asset libre_minted =
            asset( (uint64_t)( libre * SUPPORTED_TOKEN_SYMBOL_PRECISION_MULTIPLIER ), SUPPORTED_TOKEN_SYMBOL );
        save_stake( itr->account, itr->staked_days, libre_minted, itr->mint_bonus );

        mint_table.modify( *itr, get_self(), [&]( auto &ref ) {
            ref.libre_minted = libre_minted;
            ref.status = mint_status::MINT_COMPLETED;
        } );

        if ( index >= 10 )
        {
            break;
        }
    }
}

void stakingtoken::ontransfer( name from, name to, asset quantity, string memo )
{
    // skip transactions that are not related
    if ( from == get_self() || to != get_self() )
    {
        return;
    }

    bool is_stake = SUPPORTED_TOKEN_CONTRACT == get_first_receiver() && SUPPORTED_TOKEN_SYMBOL == quantity.symbol;
    bool is_mint =
        SUPPORTED_MINT_TOKEN_CONTRACT == get_first_receiver() && SUPPORTED_MINT_TOKEN_SYMBOL == quantity.symbol;

    // skip transactions from not supported contract or symbol
    if ( !is_stake && !is_mint )
    {
        return;
    }

    vector< string > params = get_params( memo );

    if ( params[0] == "stakefor" )
    {
        // contract name must match the supported token contract
        check( SUPPORTED_TOKEN_CONTRACT == get_first_receiver(), "Invalid contract" );

        save_stake( from, stoi( params[1] ), quantity, 0 );

        return;
    }

    if ( params[0] == "contributefor" )
    {
        save_mint( from, stoi( params[1] ), quantity );

        return;
    }

    check( false, "invalid memo" );
}

vector< string > stakingtoken::get_params( string memo )
{
    vector< string > params;
    string           param;
    size_t           pos = 0;
    string           delimiter = ":";

    while ( ( pos = memo.find( delimiter ) ) != string::npos )
    {
        param = memo.substr( 0, pos );
        params.push_back( param );
        memo.erase( 0, pos + delimiter.length() );
    }

    if ( memo.length() > 0 )
    {
        params.push_back( memo );
    }

    return params;
}

void stakingtoken::save_stake( name account, uint64_t stake_length, asset libre_staked, float mint_bonus )
{

    // libre_staked symbol must match the supported token symbol
    check( libre_staked.symbol == SUPPORTED_TOKEN_SYMBOL, "Invalid token symbol" );

    // stake_length must be at least 1
    // check( stake_length >= 1, "You must stake for at least 1 day" );

    // stake_length must be at least 1
    check( stake_length <= 1460, "You can stake up to 4 years" );

    // @todo: validate the current date must be equal or higher than Launch Date

    const time_point_sec stake_date = current_time_point();
    const float          apy = calculate_apy( stake_date, stake_length, mint_bonus / 100 );
    const asset          payout = calculate_payout( apy, stake_length, libre_staked );
    const time_point_sec payout_date = stake_date + days( stake_length );

    // storage the stake info
    stake_table table( get_self(), get_self().value );
    table.emplace( get_self(), [&]( auto &row ) {
        row.index = table.available_primary_key();
        row.account = account;
        row.stake_date = stake_date;
        row.stake_length = stake_length;
        row.mint_bonus = mint_bonus;
        row.libre_staked = libre_staked;
        row.apy = apy * 100;
        row.payout = payout;
        row.payout_date = payout_date;
        row.status = stake_status::STAKE_IN_PROGRESS;
    } );

    // log the stake info
    SEND_INLINE_ACTION( *this,
                        stakelog,
                        { { get_self(), name( "active" ) } },
                        { account,
                          stake_date,
                          stake_length,
                          mint_bonus,
                          libre_staked,
                          apy * 100,
                          payout,
                          payout_date,
                          stake_status::STAKE_IN_PROGRESS } );

    action( permission_level{ get_self(), eosio::name( "active" ) },
            eosio::name( "eosio" ),
            eosio::name( "vonstake" ),
            std::make_tuple( account ) )
        .send();
}

float stakingtoken::calculate_apy( time_point_sec stake_date, uint64_t stake_length, float mint_bonus )
{
    return ( ALPHA0 + sqrt( (float)stake_length / 365 ) * ( BETA0 - ALPHA0 ) +
             ( ALPHAT + sqrt( (float)stake_length / 365 ) * ( BETAT - ALPHAT ) -
               ( ALPHA0 + sqrt( (float)stake_length / 365 ) * ( BETA0 - ALPHA0 ) ) ) *
                 sqrt( min( (float)1, get_days_diff( stake_date, S_L ) / T ) ) ) *
           ( 1 + mint_bonus );
}

void stakingtoken::save_mint( name account, uint64_t mint_length, asset btc_contributed )
{
    // contract name must match the supported token contract
    check( SUPPORTED_MINT_TOKEN_CONTRACT == get_first_receiver(), "Invalid contract" );

    // btc_contributed symbol must match the supported token symbol
    check( btc_contributed.symbol == SUPPORTED_MINT_TOKEN_SYMBOL, "Invalid token symbol" );

    // mint_length must be at least 1
    check( mint_length >= 1, "You must stake for at least 1 day" );

    // mint_length must be at least 1
    check( mint_length <= 1460, "You can stake up to 4 years" );

    const time_point_sec mint_date = current_time_point();
    const float          day_of_mint_rush = get_days_diff( mint_date, M_L );

    // day_of_mint_rush must be 30 or less
    check( day_of_mint_rush <= 30, "Mint it's only available for the first 30 days" );

    const float mint_bonus = calculate_mint_bonus( day_of_mint_rush );
    const float mint_apy = calculate_mint_apy( mint_length, mint_bonus );

    // storage the mint info
    mint_bonus_t table( get_self(), get_self().value );
    table.emplace( get_self(), [&]( auto &row ) {
        row.index = table.available_primary_key();
        row.account = account;
        row.day_of_mint_rush = day_of_mint_rush;
        row.staked_days = mint_length;
        row.btc_contributed = btc_contributed;
        row.mint_bonus = mint_bonus * 100;
        row.libre_minted = asset();
        row.mint_apy = mint_apy * 100;
        row.status = mint_status::MINT_IN_PROGRESS;
    } );

    mintstats_t stats_table( get_self(), get_self().value );
    auto        stats_info = stats_table.get_or_create( get_self() );
    asset       new_balance = stats_info.btc_contributed;

    if ( !new_balance.is_valid() )
    {

        new_balance = asset( 0, SUPPORTED_MINT_TOKEN_SYMBOL );
    }

    stats_info.btc_contributed = new_balance + btc_contributed;
    stats_table.set( stats_info, get_self() );

    // log the stake info
    SEND_INLINE_ACTION(
        *this,
        mintlog,
        { { get_self(), name( "active" ) } },
        { account, day_of_mint_rush, mint_length, btc_contributed, mint_bonus * 100, asset(), mint_apy * 100 } );
}

float stakingtoken::calculate_mint_bonus( uint64_t day_of_mint_rush )
{
    return ( (float)1 - ( (float)day_of_mint_rush / (float)30 ) );
}

float stakingtoken::calculate_mint_apy( uint64_t mint_length, float mint_bonus )
{
    return ( ( ALPHA0 + sqrt( (float)mint_length / 365 ) * ( BETA0 - ALPHA0 ) ) ) * ( 1 + mint_bonus );
}

asset stakingtoken::calculate_payout( float apy, uint64_t stake_length, asset libre_staked )
{
    float   stake_amount = (float)libre_staked.amount / SUPPORTED_TOKEN_SYMBOL_PRECISION_MULTIPLIER;
    int64_t payout_amount = (int64_t)( ( (float)stake_length / 365 ) * apy * stake_amount + stake_amount );
    asset   t = asset( payout_amount * SUPPORTED_TOKEN_SYMBOL_PRECISION_MULTIPLIER, libre_staked.symbol );

    return asset( payout_amount * SUPPORTED_TOKEN_SYMBOL_PRECISION_MULTIPLIER, libre_staked.symbol );
}

float stakingtoken::get_days_diff( time_point_sec date1, time_point_sec date2 )
{
    return (float)( date1.utc_seconds - date2.utc_seconds ) / 60 / 60 / 24;
}

ACTION stakingtoken::clear()
{
    require_auth( get_self() );

    // Delete all records in stake table
    stake_table records( get_self(), get_self().value );
    auto        record = records.begin();

    while ( record != records.end() )
    {
        record = records.erase( record );
    }

    // Delete all records in mint table
    mint_bonus_t mint_table( get_self(), get_self().value );
    auto         mint = mint_table.begin();

    while ( mint != mint_table.end() )
    {
        mint = mint_table.erase( mint );
    }

    mintstats_t stats_table( get_self(), get_self().value );
    auto        stats_info = stats_table.get_or_create( get_self() );
    stats_info.btc_contributed = asset( 0, SUPPORTED_MINT_TOKEN_SYMBOL );
    stats_table.set( stats_info, get_self() );
}
