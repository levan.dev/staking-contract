# Phoenix Chain Staking Contract

<!-- Logo -->
<p align="center">
	<a href="https://github.com/edenia/staking-contract">
		<img src="./docs/img/logo-main.png" width="300">
	</a>
</p>

<!-- Shields  -->

![](https://img.shields.io/github/license/edenia/staking-contract) ![](https://img.shields.io/badge/code%20style-standard-brightgreen.svg) ![](https://img.shields.io/badge/%E2%9C%93-collaborative_etiquette-brightgreen.svg) ![](https://img.shields.io/twitter/follow/EdeniaWeb3.svg?style=social&logo=twitter) ![](https://img.shields.io/github/forks/edenia/staking-contract?style=social)

<!-- Short Intro Text  -->

@todo: add project intro

# Project Description

@todo: add Project Description

## Situation and Approach

@todo: add Situation and Approach

## User Flow

@todo: add User Flow

## User Roles

- Token Holder

## Data Model

<p align="center">
	<img src="./docs/img/data-model.png" >
</p>

## Smart Contract Actions

| User Role    | Action | Description            | Pre-Conditions        | Post-Conditions    |
| ------------ | ------ | ---------------------- | --------------------- | ------------------ |
| Token Holder | stake  | enter staking contract | user must exist       | tokens staked      |
| Token Holder | claim  | exit staking contract  | user must have staked | user claims tokens |

## Listener

To start the participation process, the user must make a "transfer" from the "eosio.token" contract with the following memo format:

- `<action>:<total_days> = string:uint64_t`

For example:

- `stakefor:365`
- `contributefor:365`

## Build and deploy

EOSIO_CDT 1.8

Update stakingtoken.hpp to properly name token:

```
    const name   SUPPORTED_TOKEN_CONTRACT = name( "eosio.token" );
    const symbol SUPPORTED_TOKEN_SYMBOL = symbol( "LIBRE", 0 );
```

## Permissions

The `claim` action acts on behalf of `eosio` account to `issue` and `transfer` new tokens, then, is needed that `eosio` grants the privileges to a specific permission.

Next are the steps to set the permission working:

1. Create the `stake` permission in the `eosio` account:

```sh
cleos set account permission eosio stake '{"threshold": 1, "keys": [], "accounts":[{"permission":{"actor": "stake.libre", "permission":"active"}, "weight": 1}], "waits": [] }' active -p eosio@active
```

2. Grant `issue` privilege to the `stake` permission:

```sh
cleos set action permission eosio eosio.token issue stake -p eosio@active
```

3. Grant `transfer` privilege to the `stake` permission:

```sh
cleos set action permission eosio eosio.token transfer stake -p eosio@active
```

## Smart Contract Accounts

The following eosio acconts have been created for smart contracts

### Jungle3 testnet

#### [stakingtoken](https://jungle3.bloks.io/account/stakingtoken)

### Phoenix testnet

@todo: create account on Phoenix testnet

# Contributing

Please Read EOS Costa Rica's [Open Source Contributing Guidelines](https://developers.eoscostarica.io/docs/open-source-guidelines).

Please report bugs big and small by [opening an issue](https://github.com/edenia/staking-contract/issues)

# About Phoenix Chain

@todo: add About Phoenix Chain

<p align="center">
	<a href="https://github.com/edenia/staking-contract">
		<img src="./docs/img/logo-footer.png" width="300">
	</a>
</p>
<br/>
