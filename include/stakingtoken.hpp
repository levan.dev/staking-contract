/*
 *
 * @author  EOSCostaRica.io [ https://eoscostarica.io ]
 *
 * @section DESCRIPTION
 *  Header file for the declaration of all functions
 *
 *  GitHub: https://github.com/edenia/staking-contract
 *
 */
#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>

using namespace std;
using namespace eosio;

// REFERRAL CONTRACT
const name REFERRAL_CONTRACT = name( "accts.libre" );

struct referral
{
    name account;
    name referrer;

    uint64_t primary_key() const
    {
        return account.value;
    }
};
typedef multi_index< "referral"_n, referral > referral_table;

name get_referrer( name acc )
{
    referral_table _referral{ REFERRAL_CONTRACT, REFERRAL_CONTRACT.value };
    auto           referral_itr = _referral.find( acc.value );

    return referral_itr != _referral.end() ? referral_itr->referrer : eosio::name( -1 );
}
// END - REFERRAL CONTRACT

CONTRACT stakingtoken : public contract
{
  public:
    using contract::contract;

    ACTION clear();

    /**
     * setinflation
     *
     * This action sets the referral inflation percentage
     *
     * @return no return value.
     */
    ACTION setinflation( uint8_t pct );

    /**
     * claim
     *
     * This action allow claim staking rewards
     *
     * @param account
     *
     * @return no return value.
     */
    ACTION claim( name account, uint64_t index );

    /**
     * unstake
     *
     * This action allow unstake the user asset
     *
     * @param account
     * @param index
     *
     * @return no return value.
     */
    ACTION unstake( name account, uint64_t index );

    /**
     * preview
     *
     * This action give a preview if the expected staking rewards
     *
     * @param account
     *
     * @return no return value.
     */
    ACTION stakepreview( name           account,
                         time_point_sec stake_date,
                         uint64_t       stake_length,
                         float          mint_bonus,
                         asset          libre_staked );

    /**
     * log
     *
     * This action log the expected staking rewards
     *
     * @param account
     *
     * @return no return value.
     */
    ACTION stakelog( name           account,
                     time_point_sec stake_date,
                     uint64_t       stake_length,
                     float          mint_bonus,
                     asset          libre_staked,
                     float          apy,
                     asset          payout,
                     time_point_sec payout_date,
                     uint64_t       status );

    /**
     * mint preview
     *
     * This action give a preview if the expected mint rewards
     *
     * @param account
     *
     * @return no return value.
     */
    ACTION mintpreview( name account, time_point_sec mint_date, uint64_t mint_length, asset btc_contributed );

    /**
     * mint log
     *
     * This action logs the expected mint rewards
     *
     * @param account
     *
     * @return no return value.
     */
    ACTION mintlog( name     account,
                    uint64_t day_of_mint_rush,
                    uint64_t staked_days,
                    asset    btc_contributed,
                    float    mint_bonus,
                    asset    libre_minted,
                    float    mint_apy );

    /**
     * mint process
     *
     * This action log the expected mint rewards
     *
     * @return no return value.
     */
    ACTION mintprocess();

    /**
     * On Transfer
     *
     * Handle deposits for pools
     *
     * @param from
     * @param to
     * @param quantity
     * @param memo
     *
     * @return no return value.
     */
    [[eosio::on_notify( "*::transfer" )]] void ontransfer( name from, name to, asset quantity, string memo );

    vector< string > get_params( string memo );

    void save_stake( name account, uint64_t stake_length, asset libre_staked, float mint_bonus );

    float calculate_apy( time_point_sec stake_date, uint64_t stake_length, float mint_bonus );

    void save_mint( name account, uint64_t mint_length, asset btc_contributed );

    float calculate_mint_bonus( uint64_t day_of_mint_rush );

    float calculate_mint_apy( uint64_t mint_length, float mint_bonus );

    asset calculate_payout( float apy, uint64_t stake_length, asset libre_staked );

    float get_days_diff( time_point_sec date1, time_point_sec date2 );

  private:
    const name    SUPPORTED_TOKEN_CONTRACT = name( "eosio.token" );
    const symbol  SUPPORTED_TOKEN_SYMBOL = symbol( "LIBRE", 4 );
    const name    TOKEN_ISSUER = name( "eosio" );
    const uint8_t SUPPORTED_TOKEN_SYMBOL_PRECISION_MULTIPLIER = 1;
    const name    SUPPORTED_MINT_TOKEN_CONTRACT = name( "eosio.token" );
    const symbol  SUPPORTED_MINT_TOKEN_SYMBOL = symbol( "PBTC", 8 );
    const name    DAO_ACCOUNT = name( "dao.libre" );
    const name    DEFAULT_REFERRER_ACCOUNT = name( "bitcoinlibre" );
    const float   UNSTAKE_PENALTY = 0.2;
    const float   ALPHA0 = 0.1;  // minimum yield
    const float   BETA0 = 2.0;   // maximum yield
    const float   ALPHAT = 0.06; // terminal min yield
    const float   BETAT = 0.3;   // terminal max yield
    const float   T = 730.0;     // days to terminal yield
    const float   D = 30;        // days in mint rush
    const float   B = 1;         // bonus for day0 mint rush
    // const float  REF = 0.2;     // Staking referral bonus
    // const float  DAO = 0.1;     // DAO mint percentage
    // const float D = 0.07; // Direct mint bonus on long stake
    const time_point_sec S_L = time_point_sec( 1656892800 ); // Launch Date 2022-07-04 GMT
    const time_point_sec M_L = time_point_sec( 1656892800 ); // Launch Date 2022-07-04 GMT
    const float          TOTAL_LIBRE_TOKEN = 10000000000.0000;

    const name EOSIO_CONTRACT = name( "eosio" );
    const name EOSIO_CONTRACT_ACTION = name( "vonstake" );

    enum stake_status : uint64_t
    {
        STAKE_IN_PROGRESS = 1,
        STAKE_COMPLETED = 2,
        STAKE_CANCELED = 3
    };

    enum mint_status : uint64_t
    {
        MINT_IN_PROGRESS = 1,
        MINT_COMPLETED = 2
    };

    TABLE stake
    {
        uint64_t       index;
        name           account;
        time_point_sec stake_date;
        uint64_t       stake_length;
        float          mint_bonus;
        asset          libre_staked;
        float          apy;
        asset          payout;
        time_point_sec payout_date;
        uint64_t       status;

        auto primary_key() const
        {
            return index;
        }
        uint64_t by_account() const
        {
            return account.value;
        }
    };
    typedef multi_index< name( "stake" ),
                         stake,
                         indexed_by< name( "account" ), const_mem_fun< stake, uint64_t, &stake::by_account > > >
        stake_table;

    TABLE mintstats
    {
        asset btc_contributed;
    };
    typedef singleton< name( "mintstats" ), mintstats > mintstats_t;

    TABLE mintbonus
    {
        uint64_t index;
        name     account;
        uint64_t day_of_mint_rush;
        uint64_t staked_days;
        asset    btc_contributed;
        float    mint_bonus;
        asset    libre_minted;
        float    mint_apy;
        uint64_t status;

        auto primary_key() const
        {
            return index;
        }

        uint64_t by_status() const
        {
            return status;
        }
    };
    typedef multi_index< name( "mintbonus" ),
                         mintbonus,
                         indexed_by< name( "status" ), const_mem_fun< mintbonus, uint64_t, &mintbonus::by_status > > >
        mint_bonus_t;

    TABLE global
    {
        uint8_t inflation_pct;
    };
    typedef eosio::singleton< "global"_n, global > global_table;
};
